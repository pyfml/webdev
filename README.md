# Webdev
Hướng dẫn trở thành web developer.

Yêu cầu: hoàn thành (đủ bài tập) lớp Python tại pymi.vn

## Mục tiêu
- Hướng dẫn lập trình web với Python bằng TIẾNG VIẸT
- Các bài viết ở dạng markdown trong repo này, sau sẽ convert thành gitbook,
  public.

## Các kiến thức cần thiết

### Web framework
Django

TODO : an End-to-end tutorial, login, deployed

### SQL Database (postgresql)

### RESTful API
django-rest-framework
TODO

### NoSQL Database (redis)

TODO


### Front-end fundamental

### JavaScript
#### VueJS?
### CSS framework?

## What else?

## Python

Go https://pymi.vn

